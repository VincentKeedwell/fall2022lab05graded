//Vicnent Keedwell 2137034
package linearalgebra;

import static org.junit.Assert.assertEquals;
import org.junit.Test;

/**
 * Vector3dTests is a class for testing Vector3d.java
 * @author Vicnent Keedwell 2137034
 * @version 9/7/2022
 */
public class Vector3dTests {
    @Test
    public void testVector3d_ConstructorAndGetMethodReturnsCorrectValue() {
        Vector3d vec = new Vector3d(1, 2, 3);
        assertEquals(1.0, vec.getX(), 0);
        assertEquals(2.0, vec.getY(), 0);
        assertEquals(3.0, vec.getZ(), 0);
    }

    @Test
    public void testVector3d_ConstructorAndMagnitudeReturnsCorrectResult() {
        Vector3d vec = new Vector3d(4, 5, 6);
        assertEquals(Math.sqrt(77), vec.magnitude(), 0);
    }

    @Test
    public void testVector3d_ConstructorAndDotProductReturnsCorrectResult() {
        Vector3d vec1 = new Vector3d(1, 2, 3);
        Vector3d vec2 = new Vector3d(4, 5, 6);
        assertEquals(32, vec1.dotProduct(vec2), 0);
    }

    @Test
    public void testVector3d_ConstructorAndAddReturnsCorrectResult() {
        Vector3d vec1 = new Vector3d(1, 2, 3);
        Vector3d vec2 = new Vector3d(4, 5, 6);
        Vector3d vec3 = vec1.add(vec2);
        assertEquals(5, vec3.getX(), 0);
        assertEquals(7, vec3.getY(), 0);
        assertEquals(9, vec3.getZ(), 0);
    }

    @Test
    public void testVector3d_ConstructorAndMagnitudeReturnsIncorrectResult() {
        Vector3d vec = new Vector3d(1, 2, 3);
        assertEquals(Math.sqrt(14), vec.magnitude(), 0);
    }
}
