package linearalgebra;

public class Vector3dTest {
    public static void main(String[] args) {
        Vector3d testVec = new Vector3d(3, 4, 5);
        Vector3d testVec2 = new Vector3d(6, 7, 8);

        System.out.println(testVec);
        System.out.println(testVec.magnitude());
        System.out.println(testVec.dotProduct(testVec2));
        System.out.println(testVec.add(testVec2));
    }
}
