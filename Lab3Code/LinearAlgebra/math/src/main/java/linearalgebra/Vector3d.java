//Vincent Keedwell 2137034
package linearalgebra;

/**
 * Vector3d is a class for making and using vectors
 * @author Vincent Keedwell 21137034
 * @version 9/7/2022
 */
public class Vector3d {
    private double x;
    private double y;
    private double z;

    public Vector3d(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public double getX() {
        return this.x;
    }
    public double getY() {
        return this.y;
    }
    public double getZ() {
        return this.z;
    }

    public String toString() {
        return (this.x + " " + this.y + " " + this.z);
    }

    public double magnitude() {
        double magni = Math.pow(this.x, 2) + Math.pow(this.y, 2) + Math.pow(this.z, 2);
        return (Math.sqrt(magni)); 
    }

    public double dotProduct(Vector3d vec) {
        double dotProd = (this.x * vec.x) + (this.y * vec.y) + (this.z * vec.z);
        return dotProd;
    }

    public Vector3d add(Vector3d vec) {
        Vector3d newVec = new Vector3d(this.x + vec.x, this.y + vec.y, this.z + vec.z);
        return newVec;
    }

    public boolean equals(Vector3d vec) {
        return this.x == vec.x && this.y == vec.y && this.z == vec.z;
    }
}